#!/usr/bin/env python3

# ---------------------------
# projects/collatz/Collatz.py
# Copyright (C) 2016
# Glenn P. Downing
# ---------------------------

# ------------
# collatz_read
# ------------


def collatz_read(s):
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]

# ------------
# collatz_eval
# ------------


def collatz_eval(i, j):
    """
    i the beginning of the range, inclusive
    j the end of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    # <your code>
    cycle_length_cache = {}
    a = [i, j]
    i = min(a)
    j = max(a)

    assert (i >= 0 and j >= 0), "Negative integer"

    for k in range(i, j+1):
        if k in cycle_length_cache:
            continue

        # set counter and given key
        num = k
        cycle_length = 1

        while k != 1:
            cycle_length += 1
            # needs to compute cycle length for key
            if k % 2 == 0:
                k = k // 2
            else:
                k = k*3 + 1

            # cycle length exist for given key
            if k in cycle_length_cache:
                cycle_length += cycle_length_cache[k]-1
                cycle_length_cache[num] = cycle_length
                break

        cycle_length_cache[num] = cycle_length

    assert len(cycle_length_cache) != 0, "List is empty"
    assert max(cycle_length_cache) > 0, "Max lenght is not greater than 0"

    return max(cycle_length_cache.values())

# -------------
# collatz_print
# -------------


def collatz_print(w, i, j, v):
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(r, w):
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
